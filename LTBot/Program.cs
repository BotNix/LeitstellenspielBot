﻿using LT;
using LT.Bots;

using System;

namespace LTBot
{
  /// <summary>
  /// Shows the program startup.
  /// </summary>
  public sealed class Program {
    #region Internal
    #region Methods
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Message">Display message. See <see cref="System.String"/>.</param>
    /// <param name="Value">Returns inputted value. See <see cref="System.String"/>.</param>
    private static void InputField(string Message, out string Value) {
      Console.Write($"{Message}: ");
      Value = Console.ReadLine();
    }
    #endregion

    #region Variables
    /// <summary>
    /// Contains object instance of <see cref="System.Boolean"/>.
    /// </summary>
    private static bool BotInitialized { get; set; } = false;
    #endregion
    #endregion

    #region Public
    #region Methods
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    /// <param name="Args"><see cref="System.String[]"/></param>
    [STAThread]
    static void Main(string[] args) {
      while (true) {
        InputField("Username", out string InputUsername);
        InputField("Password", out string InputPassword);

        if (!BotInitialized && !String.IsNullOrEmpty(InputUsername) && !String.IsNullOrEmpty(InputPassword)) {
          // change bot-initialized state
          BotInitialized = true;

          // declare configuration variable
          Settings UserSettings = new Settings();
          // apply user-data
          UserSettings.User.Username = InputUsername;
          UserSettings.User.Password = InputPassword;

          async void Init() {
            // start bot
            EmergencyBot Bot = new EmergencyBot();
            await Bot.Execute(UserSettings);
          };
          Init();
        } // end statement
      } // end while-loop
    }
    #endregion
    #endregion
  }
}
