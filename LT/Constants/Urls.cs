﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LT.Constants
{
    public class Urls
    {
        public static string Login = "https://www.leitstellenspiel.de/users/sign_in";
        public static string MainPage = "https://www.leitstellenspiel.de/";
        public static string Mission = "https://www.leitstellenspiel.de/missions/";
    }
}
